<?php

namespace App\Serializer;

use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 *   Adds the get-admin group to the context if the authenticated used has ROLE_ADMIN
 * and if class being serialized/deserialized is App\Entity\User
 *
 * For this to work add the following to definition to services.yaml
 *     App\Serializer\UserContextBuilder:
 *       decorates: 'api_platform.serializer.context_builder'
 *       arguments: ['@App\Serializer\UserContextBuilder.inner']
 */
class UserContextBuilder implements SerializerContextBuilderInterface
{

    private SerializerContextBuilderInterface $decorated;
    private AuthorizationCheckerInterface $authorizationChecker;

    public function __construct(
        SerializerContextBuilderInterface $decorated,
        AuthorizationCheckerInterface     $authorizationChecker
    )
    {
        $this->decorated = $decorated;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createFromRequest(
        Request $request, bool $normalization, array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);

        // Class being serialized/deserialized
        $resourceClass = $context['resource_class'] ?? null; // Default to null if not set

        if (
            User::class === $resourceClass &&
            isset($context['groups']) &&
            $normalization === true &&
            $this->authorizationChecker->isGranted(User::ROLE_ADMIN)
        ) {
            $context['groups'][] = 'get-admin';
        }

        return $context;
    }
}