<?php

namespace App\DataFixtures;

use App\Entity\BlogPost;
use App\Entity\Comment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    private UserPasswordHasherInterface $userPasswordHasher;
    private Generator $faker;

    private const USERS = [
        [
            'username' => 'admin',
            'email' => 'admin@blog.com',
            'name' => 'Bruno Jesus',
            'password' => 'secret123#',
            'roles' => [User::ROLE_SUPERADMIN]
        ],
        [
            'username' => 'john_doe',
            'email' => 'john@blog.com',
            'name' => 'John Doe',
            'password' => 'secret123#',
            'roles' => [User::ROLE_ADMIN]
        ],
        [
            'username' => 'rob_smith',
            'email' => 'rob@blog.com',
            'name' => 'Rob Smith',
            'password' => 'secret123#',
            'roles' => [User::ROLE_WRITER]
        ],
        [
            'username' => 'jenny_rowling',
            'email' => 'jenny@blog.com',
            'name' => 'Jenny Rowling',
            'password' => 'secret123#',
            'roles' => [User::ROLE_WRITER]
        ],
        [
            'username' => 'han_solo',
            'email' => 'han@blog.com',
            'name' => 'Han Solo',
            'password' => 'secret123#',
            'roles' => [User::ROLE_EDITOR]
        ],
        [
            'username' => 'jedi_knight',
            'email' => 'jedi@blog.com',
            'name' => 'Jedi Knight',
            'password' => 'secret123#',
            'roles' => [User::ROLE_COMMENTATOR]
        ]
    ];

    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
        $this->faker = Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadUsers($manager);
        $this->loadBlogPosts($manager);
        $this->loadComments($manager);
    }

    public function loadBlogPosts(ObjectManager $manager)
    {
        for ($i = 0; $i < 100; $i++) {
            $blogPost = new BlogPost();
            $blogPost->setTitle($this->faker->realText(30));
            $blogPost->setPublished($this->faker->dateTimeThisYear());
            $blogPost->setContent($this->faker->realText());
            $blogPost->setAuthor($this->getRandomUserReference($blogPost));
            $blogPost->setSlug($this->faker->slug());

            $this->setReference("blog_post_$i", $blogPost);
            $manager->persist($blogPost);
        }

        $manager->flush();
    }

    public function loadComments(ObjectManager $manager)
    {
        /** @var User $user */
        for ($i = 0; $i < 100; $i++) {
            for ($j = 0; $j < rand(1, 10); $j++) {
                $comment = new Comment();
                $comment->setContent($this->faker->realText())
                    ->setPublished($this->faker->dateTimeThisYear())
                    ->setAuthor($this->getRandomUserReference($comment))
                    ->setBlogPost($this->getReference("blog_post_$i"));

                $manager->persist($comment);
            }
        }

        $manager->flush();
    }

    public function loadUsers(ObjectManager $manager)
    {
        foreach (self::USERS as $user) {
            $entity = new User();
            $entity->setUsername($user['username'])
                ->setEmail($user['email'])
                ->setName($user['name'])
                ->setRoles($user['roles']);

            $entity->setPassword($this->userPasswordHasher->hashPassword(
                $entity,
                $user['password']
            ));

            $this->addReference('user_' . $user['username'], $entity);

            $manager->persist($entity);
        }

        $manager->flush();
    }

    /**
     * @return User
     */
    public function getRandomUserReference($entity): User
    {
        $randomUser = self::USERS[rand(0, 5)];

        $randomUserRoles = $randomUser['roles'];
        $blogPostRoles = [User::ROLE_SUPERADMIN, User::ROLE_ADMIN, User::ROLE_WRITER];
        $commentRoles = array_merge($blogPostRoles, [User::ROLE_COMMENTATOR]);

        if ($entity instanceof BlogPost &&
            !$this->hasIntersection($randomUserRoles, $blogPostRoles)
        ) {
            return $this->getRandomUserReference($entity);
        }

        if ($entity instanceof Comment &&
            !$this->hasIntersection($randomUserRoles, $commentRoles)
        ) {
            return $this->getRandomUserReference($entity);
        }

        /** @var User $user */
        $user = $this->getReference("user_" . $randomUser['username']);

        return $user;
    }

    private function hasIntersection(array $array1, array $array2): bool
    {
        return count(array_intersect($array1, $array2)) > 0;
    }
}
