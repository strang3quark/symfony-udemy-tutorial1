<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\PublishedDateEntityInterface;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class PublishedDateEntitySubscriber implements EventSubscriberInterface
{

    #[ArrayShape([KernelEvents::VIEW => "array"])]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['setDatePublished', EventPriorities::PRE_WRITE]
        ];
    }

    public function setDatePublished(ViewEvent $event)
    {
        /** @var PublishedDateEntityInterface $entity  */
        $entity = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        $allowedMethods = [Request::METHOD_POST];
        $isValidMethod = in_array($method, $allowedMethods);

        if ($entity instanceof PublishedDateEntityInterface && $isValidMethod) {
            $entity->setPublished(new \DateTime());
        }
    }
}