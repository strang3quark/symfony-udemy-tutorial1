<?php

namespace App\Controller;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Action for resetting password using api controller
 */
class ResetPasswordAction
{

    private ValidatorInterface $validator;
    private UserPasswordHasherInterface $userPasswordHasher;
    private EntityManagerInterface $entityManager;
    private JWTTokenManagerInterface $tokenManager;

    /**
     * @param ValidatorInterface $validator
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ValidatorInterface          $validator,
        UserPasswordHasherInterface $userPasswordHasher,
        EntityManagerInterface      $entityManager,
        JWTTokenManagerInterface    $tokenManager
    )
    {
        $this->validator = $validator;
        $this->userPasswordHasher = $userPasswordHasher;
        $this->entityManager = $entityManager;
        $this->tokenManager = $tokenManager;
    }

    public function __invoke(User $data): JsonResponse
    {
        // Allows using class as a method
        // $reset = new ResetPasswordAction();
        // $reset();

        $this->validator->validate($data);

        $data->setPassword(
            $this->userPasswordHasher->hashPassword(
                $data, $data->getNewPassword()
            )
        );

        // We store the password change date in order to have a way to invalidate old tokens
        $data->setPasswordChangeDate(time());

        $this->entityManager->flush();

        $token = $this->tokenManager->create($data);

        return new JsonResponse(['token' => $token]);
        // Validator is only called after we return the data from this action!
    }
}